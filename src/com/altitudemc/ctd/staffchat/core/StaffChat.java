package com.altitudemc.ctd.staffchat.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import com.altitudemc.ctd.staffchat.utils.ChatUtil;

public class StaffChat {
	
	private StaffChatMain plugin;
	private final String PREFIX = "&9[STAFF]&f ";
	private List<UUID> toggled;

	public StaffChat(StaffChatMain plugin) {
		this.plugin = plugin;
		toggled = new ArrayList<UUID>();
	}
	
	public void sendChat(String message) {
		for (ProxiedPlayer player : plugin.getProxy().getPlayers()) {
			if (player.hasPermission("staffchat.chat") && !toggled.contains(player.getUniqueId())) {
				player.sendMessage(TextComponent.fromLegacyText((ChatUtil.colorize(PREFIX + message))));
			}
		}
	}
	
	public void sendSystemAlert(String message) {
		sendChat("&c[SYSTEM]&f " + message);
	}
	
	public void sendReport(String message) {
		sendChat("&a[REPORT]&f " + message);
	}
	
	public boolean hasChatDisabled(ProxiedPlayer player) {
		return toggled.contains(player.getUniqueId());
	}
	
	public void toggleChat(ProxiedPlayer player) {
		if (hasChatDisabled(player)) {
			toggled.remove(player.getUniqueId());
		} else {
			toggled.add(player.getUniqueId());
		}
	}
	
	public void sendConnectMessage(ProxiedPlayer player) {
		sendChat(ChatColor.YELLOW + player.getDisplayName() + " &ehas connected.");
	}
	
	public void sendDisconnectMessage(ProxiedPlayer player) {
		sendChat(ChatColor.YELLOW + player.getDisplayName() + " &ehas disconnected.");
	}
}
