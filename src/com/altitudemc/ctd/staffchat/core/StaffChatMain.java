package com.altitudemc.ctd.staffchat.core;

import net.md_5.bungee.api.plugin.Plugin;

import com.altitudemc.ctd.staffchat.commands.ReportCommand;
import com.altitudemc.ctd.staffchat.commands.StaffChatCommand;
import com.altitudemc.ctd.staffchat.commands.StaffHelpCommand;
import com.altitudemc.ctd.staffchat.commands.StaffListCommand;
import com.altitudemc.ctd.staffchat.commands.SystemCommand;
import com.altitudemc.ctd.staffchat.commands.ToggleCommand;
import com.altitudemc.ctd.staffchat.listeners.PlayerJoin;
import com.altitudemc.ctd.staffchat.listeners.PlayerLeave;

/**
 * Staff chat plugin for Bungee
 * 
 * @author ctd
 * @version 1.0
 */
public class StaffChatMain extends Plugin {
	
	private StaffChat staffChat;
	
	public void onEnable() {
		staffChat = new StaffChat(this);
		registerCommands();
		registerListeners();
	}
	
	public void onDisable() {
		
	}
	
	public void registerCommands() {
		getProxy().getPluginManager().registerCommand(this, new StaffChatCommand(this));
		getProxy().getPluginManager().registerCommand(this, new ReportCommand(this));
		getProxy().getPluginManager().registerCommand(this, new StaffListCommand(this));
		getProxy().getPluginManager().registerCommand(this, new SystemCommand(this));
		getProxy().getPluginManager().registerCommand(this, new ToggleCommand(this));
		getProxy().getPluginManager().registerCommand(this, new StaffHelpCommand(this));
	}
	
	public void registerListeners() {
		getProxy().getPluginManager().registerListener(this, new PlayerJoin(this));
		getProxy().getPluginManager().registerListener(this, new PlayerLeave(this));
	}
	
	public StaffChat getStaffChat() {
		return staffChat;
	}
}
