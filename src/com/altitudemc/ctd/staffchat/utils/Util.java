package com.altitudemc.ctd.staffchat.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Util {
	
	/**
	 * Check if a user is currently connected to the bungee
	 * 
	 * @param username
	 *            The username to find
	 * @return if they're online or not
	 */
	public static boolean isOnline(String username) {
		for (ProxiedPlayer player : BungeeCord.getInstance().getPlayers()) {
			if (player.getName().equalsIgnoreCase(username)) {
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Check if a user is currently connected to the bungee
	 * 
	 * @param username The username to find
	 * @return if they're online or not
	 */
	public static boolean isOnline(UUID uuid) {
		for (ProxiedPlayer player : BungeeCord.getInstance().getPlayers()) {
			if (player.getUniqueId().equals(uuid)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get an online player based on their username
	 * 
	 * @param username
	 *            the username to search
	 * @return the player
	 */
	public static ProxiedPlayer getPlayer(String username) {
		ProxiedPlayer target = null;

		for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
			if (p.getName().equalsIgnoreCase(username)) {
				target = p;
				break;
			}
		}

		return target;
	}
	
	/**
	 * Get an online player based on their username
	 * 
	 * @param uuid the player's uuid
	 * @return the player
	 */
	public static ProxiedPlayer getPlayer(UUID uuid) {
		ProxiedPlayer target = null;

		for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
			if (p.getUniqueId().equals(uuid)) {
				target = p;
				break;
			}
		}

		return target;
	}
	
	public static String argsToString(String[] args) {
		return argsToString(args, 0);
	}

	public static String argsToString(String[] args, int start) {
		String concat = "";

		for (int i = start; i < args.length; i++) {
			concat += args[i] + " ";
		}

		return concat.trim();
	}
	
	public static String stripRank(String displayName) {
		if (displayName.startsWith("[") && displayName.contains("]"))
			return displayName.split("]")[1].trim();
		else
			return displayName;
	}
	
	public static List<UUID> getOnlineStaff() {
		List<UUID> staff = new ArrayList<UUID>();
		
		for (ProxiedPlayer player : BungeeCord.getInstance().getPlayers()) {
			if (player.hasPermission("staffchat.chat")) {
				staff.add(player.getUniqueId());
			}
		}
		
		return staff;
	}
	
	public static boolean hasStaffOnline() {
		return getOnlineStaff().size() > 0;
	}
}
