package com.altitudemc.ctd.staffchat.utils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ChatUtil {
	
	public static String colorize(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}
	
	public static void sendMessage(ProxiedPlayer player, String message) {
		player.sendMessage(TextComponent.fromLegacyText(colorize(message)));
	}
	
	public static void sendMessage(CommandSender sender, String message) {
		sender.sendMessage(TextComponent.fromLegacyText(colorize(message)));
	}

	public static String argsToString(String[] args) {
		return argsToString(args, 0);
	}

	public static String argsToString(String[] args, int start) {
		String concat = "";

		for (int i = start; i < args.length; i++) {
			concat += args[i] + " ";
		}

		return concat.trim();
	}
}
