package com.altitudemc.ctd.staffchat.listeners;

import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import com.altitudemc.ctd.staffchat.core.StaffChatMain;

public class PlayerJoin implements Listener {
	
	private StaffChatMain plugin;

	public PlayerJoin(StaffChatMain plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerJoin(PostLoginEvent event) {
		final ProxiedPlayer player = event.getPlayer();
		
		if (player.hasPermission("staffchat.chat")) {
			// Delay the message so another plugin can set their displayname
			ProxyServer.getInstance().getScheduler().schedule(plugin, new Runnable() {
				public void run() {
					plugin.getStaffChat().sendConnectMessage(player);
				}
			}, 2, TimeUnit.SECONDS);
		}
	}
}
