package com.altitudemc.ctd.staffchat.listeners;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;

import com.altitudemc.ctd.staffchat.core.StaffChatMain;

public class PlayerLeave implements Listener {
	
	private StaffChatMain plugin;

	public PlayerLeave(StaffChatMain plugin) {
		this.plugin = plugin;
	}
	
	public void onPlayerJoin(PlayerDisconnectEvent event) {
		ProxiedPlayer player = event.getPlayer();
		
		if (player.hasPermission("staffchat.chat")) {
			plugin.getStaffChat().sendDisconnectMessage(player);
		}
	}
}
