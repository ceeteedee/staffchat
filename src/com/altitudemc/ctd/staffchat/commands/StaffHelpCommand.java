package com.altitudemc.ctd.staffchat.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChat;
import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;

public class StaffHelpCommand extends Command {

	private final String USAGE = "&cUsage: /staff";
	private StaffChatMain plugin;
	
	public StaffHelpCommand(StaffChatMain plugin) {
		super("staff", "staffchat.chat");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer player = (ProxiedPlayer) sender;
			
			if (args.length == 0) {
				ChatUtil.sendMessage(player, "&9-------------------&bStaff Commands&9---------------------");
				ChatUtil.sendMessage(player, "&b/<s | sc> <message...> - &7Send a message to all staff");
				ChatUtil.sendMessage(player, "&b/report <username> <reason> - &7Report a player to all staff");
				ChatUtil.sendMessage(player, "&b/sctoggle - &7Toggles the staff chat");
				ChatUtil.sendMessage(player, "&b/<stafflist | sl> - &7Lists all online staff");
				ChatUtil.sendMessage(player, "&b/sys <message...> - &7Send a System alert to all staff");
				ChatUtil.sendMessage(player, "&9-----------------------------------------------------");
			} else {
				ChatUtil.sendMessage(player, USAGE);
			}
		}
	}
}
