package com.altitudemc.ctd.staffchat.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChat;
import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;

public class StaffChatCommand extends Command {

	private final String USAGE = "&cUsage: /<s | sc> <message...>";
	private StaffChatMain plugin;
	
	public StaffChatCommand(StaffChatMain plugin) {
		super("s", "staffchat.chat", "sc");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		StaffChat staffChat = plugin.getStaffChat();
		String message = ChatUtil.argsToString(args);
		
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer player = (ProxiedPlayer) sender;
			
			if (args.length > 0) {
				staffChat.sendChat(player.getDisplayName() + ": &f" + message);
			} else {
				ChatUtil.sendMessage(player, USAGE);
			}
		} else {
			staffChat.sendSystemAlert(message);
		}
	}
}
