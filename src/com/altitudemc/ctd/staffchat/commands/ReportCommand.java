package com.altitudemc.ctd.staffchat.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChat;
import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;
import com.altitudemc.ctd.staffchat.utils.Util;

public class ReportCommand extends Command {

	private final String USAGE = "&cUsage: /report <username> <reason>";
	private StaffChatMain plugin;
	
	public ReportCommand(StaffChatMain plugin) {
		super("report", "staffchat.chat");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer player = (ProxiedPlayer) sender;
			ProxiedPlayer target;
			StaffChat staffChat = plugin.getStaffChat();
			String message;
			
			if (args.length > 1) {
				message = ChatUtil.argsToString(args, 1).trim();
				
				if (Util.isOnline(args[0])) {
					target = Util.getPlayer(args[0]);
					staffChat.sendReport(player.getDisplayName() + "&f: Name: " + target.getDisplayName() + "&f - " + message);
				} else {
					ChatUtil.sendMessage(player, "&cThis player is not online!");
				}
			} else {
				ChatUtil.sendMessage(player, USAGE);
			}
		} else {
			ChatUtil.sendMessage(sender, "&cYou cannot use this command from the console!");
		}
	}
}
