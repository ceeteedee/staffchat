package com.altitudemc.ctd.staffchat.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChat;
import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;

public class ToggleCommand extends Command {

	private StaffChatMain plugin;
	private final String USAGE = "&cUsage: /sctoggle";

	public ToggleCommand(StaffChatMain plugin) {
		super("sctoggle", "staffchat.chat");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		StaffChat staffChat = plugin.getStaffChat();
		ProxiedPlayer player;
		
		if (sender instanceof ProxiedPlayer) {
			player = (ProxiedPlayer) sender;
			if (args.length == 0) {
				staffChat.toggleChat(player);
				
				if (staffChat.hasChatDisabled(player)) {
					ChatUtil.sendMessage(player, "&cStaff chat has been disabled!");
				} else {
					ChatUtil.sendMessage(player, "&aStaff chat has been enabled!");
				}
			} else {
				ChatUtil.sendMessage(player, USAGE);
			}
		} else {
			ChatUtil.sendMessage(sender, "&cYou cannot use this command from the console!");
		}
	}
}
