package com.altitudemc.ctd.staffchat.commands;

import java.util.UUID;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;
import com.altitudemc.ctd.staffchat.utils.Util;

public class StaffListCommand extends Command {

	private final String USAGE = "&cUsage: /<stafflist | sl>";
	private StaffChatMain plugin;
	
	public StaffListCommand(StaffChatMain plugin) {
		super("stafflist", "staffchat.chat", "sl");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		ProxiedPlayer staffMember;
		String staffList = "";
		
		if (args.length == 0) {
			if (Util.hasStaffOnline()) {
				for (UUID uuid : Util.getOnlineStaff()) {
					staffMember = Util.getPlayer(uuid);
					staffList += staffMember.getDisplayName() + " &e- &a" + staffMember.getServer().getInfo().getName() + "\n";
				}
	
				ChatUtil.sendMessage(sender, "&aOnline staff\n" + staffList.substring(0, staffList.length() - 1));
			} else {
				ChatUtil.sendMessage(sender, "&aOnline staff\n&cNone");
			}
		} else {
			ChatUtil.sendMessage(sender, USAGE);
		}
	}
}
