package com.altitudemc.ctd.staffchat.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import com.altitudemc.ctd.staffchat.core.StaffChat;
import com.altitudemc.ctd.staffchat.core.StaffChatMain;
import com.altitudemc.ctd.staffchat.utils.ChatUtil;

public class SystemCommand extends Command {

	private StaffChatMain plugin;
	private final String USAGE = "&cUsage: /sys <message...>";
	
	public SystemCommand(StaffChatMain plugin) {
		super("system", "staffchat.system", "sys");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		StaffChat staffChat = plugin.getStaffChat();
		String message = ChatUtil.argsToString(args);
		
		if (args.length > 0) {
			staffChat.sendSystemAlert(message);
		} else {
			ChatUtil.sendMessage(sender, USAGE);
		}
	}
}
